<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    private $title;
    private $price;
    private $pagesNumber;

    public function __construct(
        string $title,
        int $price,
        int $pagesNumber
    ) {
        $this->title = $title;

        if ($price < 0) {
            throw new \Exception("Price should be positive number");
        }
        $this->price = $price;

        if ($pagesNumber < 0) {
            throw new \Exception("PagesNumber should be positive number");
        }
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}