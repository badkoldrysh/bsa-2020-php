<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    private $id;
    private $image;
    private $name;
    private $speed;
    private $pitStopTime;
    private $fuelConsumption;
    private $fuelTankVolume;

    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        $this->id = $id;
        $this->image = $image;
        $this->name = $name;

        if ($speed < 0) {
            throw new \Exception("Speed cannot be negative. So you can try to increase it");
        }
        $this->speed = $speed;

        if ($pitStopTime < 0) {
            throw new \Exception("PitStopTime cannot be negative");
        }
        $this->pitStopTime = $pitStopTime;

        if ($fuelConsumption < 0) {
            throw new \Exception("Fuel consumption should be positive");
        }
        $this->fuelConsumption = $fuelConsumption;

        if ($fuelTankVolume < 0) {
            throw new \Exception("FuelTankVolume should be positive");
        }
        $this->fuelTankVolume = $fuelTankVolume;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}