<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private $lapLength;
    private $lapsNumber;
    private $cars;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ($lapLength < 0) {
            throw new \Exception("LapLength cannot be negative");
        }
        $this->lapLength = $lapLength;

        if ($lapsNumber < 0) {
            throw new \Exception("LapsNumber cannot be negative");
        }
        $this->lapsNumber = $lapsNumber;
        $this->cars = [];
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        array_push($this->cars, $car);

        return;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        if (empty($this->cars)) {
            throw new \Exception("You need add cars to the track");
        }

        $distance = $this->lapLength * $this->lapsNumber;

        foreach ($this->cars as $car) {
            $timeWithoutPitstops = $distance / $car->getSpeed();

            $requiredFuel = $distance * $car->getFuelConsumption() / 100;
            $pitstops =  (int) ($requiredFuel / $car->getFuelTankVolume());
            $pauseTime = $pitstops * $car->getPitStopTime();
            $finishTime = $timeWithoutPitstops + $pauseTime / 3600;

            if (isset($bestTime) === false || $finishTime < $bestTime) {
                $bestTime = $finishTime;
                $champion = $car;
            }

        }

        return $champion;
    }
}