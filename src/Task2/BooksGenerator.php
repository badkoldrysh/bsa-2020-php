<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private $libraryBooks;
    private $storeBooks;
    private $maxPrice;
    private $minPagesNumber;

    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    ) {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $libBook) {
            if ($libBook->getPagesNumber() >= $this->minPagesNumber) {
                yield $libBook;
            }
        }

        foreach ($this->storeBooks as $storeBook) {
            if ($storeBook->getPrice() <= $this->maxPrice) {
                yield $storeBook;
            }
        }
    }
}
