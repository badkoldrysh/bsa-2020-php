<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $html = '<div class="wrapper">';

        $cars = $track->all();

        foreach ($cars as $car) {
            $html .= <<<HTML
                <div>
                    <img src="{$car->getImage()}">
                    <p>{$car->getName()}: {$car->getSpeed()}, {$car->getFuelConsumption()}</p>
                </div>
            HTML;
        }

        $html .= "</div>";
        return $html;
    }
}
